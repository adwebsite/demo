<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Voerro\FileUploader\FileUploader;
use App\Language;

class HeroWeapon extends Model
{
   protected $table = 'hero_weapons';

   public $timestamps = false;

   protected $fillable = [        
        'hero_id',		
		'title',
		'text',
		'image'		
    ];

	protected static function boot()
    {
        parent::boot();
		
		// Если это изображение - то удаление
		static::deleting(function ($model) {			
			FileUploader::delete($model->image);			
		});		
    }
	
}
