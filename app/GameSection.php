<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Voerro\FileUploader\FileUploader;
use App\GameComplexItem;

class GameSection extends Model
{
    protected $fillable = [
		'game_id',
        'slug',
		'name'        
    ];
	
	public $timestamps = false;
    
	public function items()
    {
        return $this->hasMany('App\GameComplexItem', 'game_section_id')->orderBy('id','ASC');
    }	
	

	
}