<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Feedback;
use App\Bugs;
use App\User;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
		$reg = User::select(DB::raw('count(id) AS cnt, DATE_FORMAT(`created_at`,"%Y-%m-%d") AS day'))
			->orderBy('created_at','ASC')			
			->whereRaw('DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= `created_at`')
			->groupBy(DB::raw("DATE_FORMAT(`created_at`,'%Y-%m-%d')"))
			->get()
			->toArray();
			
				
        return view('dashboard.index',
		[			
			'feedback_count'	=> Feedback::count(),
			'bugs_count' 		=> Bugs::count(),
			'users_count'		=> User::where('role','user')->count(),
			'active_menu'		=> 'none',
			'registration'		=> $reg				
		]);
    }
}
