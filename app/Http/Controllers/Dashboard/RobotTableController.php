<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Traits\ApiResponses;
use App\GameRobot;
use App\RobotTable;

class RobotTableController extends Controller
{
	use ApiResponses;
	
	//кажем таблицу
	public function table(GameRobot $robot)
    {
		if (request()->wantsJson()) {
			$limit = (request()->has('limit')) ? intval(request('limit')) : 30;
			$offset = (request()->has('offset')) ? intval(request('offset')) : 0;
			$sortableFields = ['id','slug','language'];
		
			$lines = RobotTable::where('robot_id',$robot->id);
		
			$total = ($lines) ? $lines->count() : 0;
		
			$lines = $lines
				->take($limit)
				->skip($offset);
			
			
			if (request()->has('sort') && in_array(request('sort'),$sortableFields)) {
				$lines = $lines->orderby(request('sort'),request('order'));
			}
		
			$lines = $lines->get();		
		
		return json_encode([
				'rows' => $lines,
				'total' => $total
			],JSON_UNESCAPED_UNICODE);
		}
	}
	
	// форма добавления/редактирования строки таблицы
	public function show($id = null)
    {
		$columns = null;
		
		if ($id && RobotTable::where('id',$id)->exists()) {
			$columns = RobotTable::where('id',$id)->first();
		}
		
		$html = view('dashboard.mech.table.line', [			
			'edit' => $columns,
			'robot_id' => request('mech_id')
		])->render();
			
		return $this->payload($html, 201);
		/*} else {
			return response()->json('Неверные данные для запроса.', 404, [],  JSON_UNESCAPED_UNICODE);
		}*/
    }
		
	
	public function store()
    {		
		$data = request()->all();
		$params = [];
		
		parse_str($data['data'], $params);	
		$this->storeOrUpdate($params, (!empty($params['edit_id']) ? $params['edit_id'] : null));
		return redirect()->route('dashboard.game.robot.index');
    }
	
	public function destroy($id){
		if (RobotTable::find($id)->delete()) {
			return $this->payload("Успешно удален", 201);
		} else {
			return $this->errorResponse('Участник не найден.', 404);
		}			
	}
	
	
    public function storeOrUpdate($data, $edit = null)
    {		
		if ($edit) {
			// Обновление			
			if (!empty($data['line']) && is_array($data['line'])) {
				$data['columns'] = implode('|',$data['line']);
			}
			
			$robot = RobotTable::find($edit);			
			$columns = $robot->update($data);
		} else {			
			if (!empty($data['line']) && is_array($data['line'])) {
				$data['columns'] = implode('|',$data['line']);
			}
			
			$columns = RobotTable::create($data);
			// Новый
		}		
		
		return $columns;
    }   

}
