<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponses;
use App\Role;

class RolesController extends Controller
{
	use ApiResponses;
	
	// показываем список по языку
	public function index($language)
    {
		if ($roles = Role::select('key','title')
			->where('visible',1)
			->where('language',$language)
			->get()) {
			
			return response()->json($roles, 200,  [],  JSON_UNESCAPED_UNICODE);
		} else {
			return response()->json('Нет такого языка', 404, [],  JSON_UNESCAPED_UNICODE);
		}
    }

}
