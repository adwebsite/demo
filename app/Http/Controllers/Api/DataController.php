<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponses;
use App\Page;
use App\PageItem;
use App\Language;
use App\Team;
use App\Game;
use App\GameItem;
use App\GameComplexItem;
use App\Feedback;
use App\Role;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;


class DataController extends Controller
{
    use ApiResponses;
    /**
     * @api {get} /data/{slug} Получить страницу
     * @apiGroup Data
	 * @apiVersion 0.2.0
     * @apiName Получить данные страницы
     *
     * @apiParam {slug} string - служебное имя страницы, например CreateAcc
     *
     * @apiSuccessExample Success-Response
     *     HTTP/1.1 200 OK
     *	   {
     *       "status": 200,
     *       "payload": {
     *               "page": {
     *                   "id": 1,
     *                   "slug": "header",
     *                   "name": "Шапка",
     *                   "updated_at": null
     *               },
     *               "data": {
     *                   "logInText": {
     *                       "EN": [
     *                           {
     *                               "slug": "login",
     *                               "value": "login",
     *                               "language": "EN"
     *                           },
     *                           {
     *                               "slug": "refistration",
     *                               "value": "registration",
     *                               "language": "EN"
     *                           }
     *                       ],
     *                       "RU": [
     *                           {
     *                               "slug": "login",
     *                               "value": "войти",
     *                               "language": "RU"
     *                           },
     *                           {
     *                               "slug": "registration",
     *                               "value": "зарегистрироваться",
     *                               "language": "RU"
     *                           }
     *                       ]
     *                   },
	 *	           }
	 *	   }	 
	 * @apiErrorExample Error-Response
     *     HTTP/1.1 404 Forbidden
     *     {
     *        "status": 404,
     *        "payload": "Страница mymraf не найдена "
     *     }
     */
    public function fullpage($slug)
    {
        if ($page = Page::select('id','slug','name','updated_at') 
			->where('slug',$slug)
			->first()) {
			
			$data = [];
			
			if ($general_items = Page::where("page_id",$page->id)->get()) {
			
				$out = [];
				foreach ($general_items AS $sub_page) {
					$grp = 'language';					
					if ($data = PageItem::select("group_item","slug","value","href","language")
						->where('page_id',$sub_page->id)				
						->get()
						->groupby($grp)
						->toArray()
					) {						
						if (isset($data[''])) {
							$i = 0;
							foreach ($data[''] AS $e) {
								$data[''][$i]['value'] = (isset($e['value']) && $e['slug']=='img') ? asset('storage/' . $e['value']) : $e['value'];
								$i++;
							}
						}						
						$out[$sub_page->slug] = $data;
					}
				}				
			}
			
			$data = $out;
        
			return $this->payload([
				'page' => $page,
				'data' => $data
			]);
		} else {
			return $this->errorResponse("Страница $slug не найдена ", 404);
		}
    }
	
	/**
     * @api {get} /languages Список доступных языков
     * @apiGroup Data
	 * @apiVersion 0.2.0
     * @apiName Список доступных языков     
     *
     * @apiSuccessExample Success-Response
     *     HTTP/1.1 200 OK
     *     {
     *         "status": 200,
     *         "payload": {            	
	 *              "payload": [
     *                {
     *                "slug": "EN",
     *                "title": "English"
     *                },
     *                {
     *                "slug": "RU",
     *                "title": "Русский"
     *                }
     *              ]
     *	       }
     *     }
     */
    public function languages()
    {
		$lang = Language::where('slug','!=','NONE')
			->orderBy('slug','ASC')
			->get();			
		return $this->payload($lang);		
	}
	
	/**
     * @api {post} /feedback Отправка сообщений в техподдержку
     * @apiGroup Data
	 * @apiVersion 0.3.0
     * @apiName Отправка сообщений в техподдержку
	 * @apiDescription Отправка сообщений в техподдержку
	 *
	 * @apiParam {String} game_slug идентификатор Игры
	 * @apiParam {Array} questions массив выбранных вопросов ['вопрос1','вопрос2'] - сами тексты вопросов
	 * @apiParam {String} message сообщение пользователя
	 * @apiParam {String} name Имя пользователя
	 * @apiParam {String} email email пользователя
     *
     * @apiSuccessExample Success-Response
     *{
     *    "status": 200,
     *    "payload": {
     *        "EN": [
     *            {
     *                "value": "Thank's!",
     *                "language": "EN"
     *            }
     *        ],
     *        "RU": [
     *            {
     *                "value": "Спасибо, мы по возможности быстро ответим!",
     *                "language": "RU"
     *            }
     *        ]
     *    }
     *}
     */
    public function feedback()
    {	
		// Может проверка нужна
		
		$req = request()->all();
		
		$req['questions'] = (!empty(request('questions')) && is_array(request('questions'))) 
			? $req['questions'] = implode(',',request('questions'))
			: '';
		
		$message = Feedback::create($req);
		
		$response = PageItem::select('value','language')
			->where('slug','response')
			->orderBy('language','ASC')			
			->get()
			->groupby('language')
			->toArray();
		
		$admin_email = "adwebsite@ya.ru";
		
		Mail::to($admin_email)->send(new SendEmail($message));
		
		return $this->payload($response);		
	}

	
	/**
     * @api {get} /address Список адресов
     * @apiGroup Data
	 * @apiVersion 0.2.1
     * @apiName Список адресов
     *
     * @apiSuccessExample Success-Response
     *     HTTP/1.1 200 OK
     *     	{
	 *         "status": 200,
	 *             "payload": [
	 *                 {
	 *                    "id": 150,
	 *                    "language": "RU",
	 *                    "value": "Кремль",
	 *                    "country": "Россия"
	 *                },
	 *                {
	 *                    "id": 151,
	 *                    "language": "EN",
	 *                    "value": "WhiteHouse",
	 *                    "country": "USA"
	 *                }
	 *            ]
	 *      }
     */
    public function address()
    {
		$addr = PageItem::selectRaw('id,language,value,href AS country') 
			->where('slug','address')
			->get();
			
		return $this->payload($addr);		
	}

	/**
     * @api {get} /team Список участников команды
     * @apiGroup Data
	 * @apiVersion 0.2.1
     * @apiName Список участников команды
     *
     * @apiSuccessExample Success-Response
     *     HTTP/1.1 200 OK
     *     {
     *         "status": 200,
     *         "payload": {
     *                 {
     *                     "id": 9,
     *                     "sort": 0,
     *                     "image": "images/QxPTCm3pl6ENu2hBGX3k6WlY7ydVrXrJ73xt4muC.png",
     *                     "people": {
     *                         "EN": [
     *                             {
     *                                 "id": 3,
     *                                 "team_id": 9,
     *                                 "language": "EN",
     *                                 "name": "Maxim Nesterov",
     *                                 "position": "Chief design officer",
     *                                 "make": "3d Character, 3d Environment, Hardsurface Texture modelling\"",
     *                                 "platforms": "for PC, Playstation 4, Nintendo, Switch/DS, Xbox One, Mobile"
     *                             }
     *                         ],
     *                         "RU": [
     *                             {
     *                                 "id": 4,
     *                                 "team_id": 9,
     *                                 "language": "RU",
     *                                 "name": "Максим Нестеров",
     *                                 "position": "Главный конструктор",
     *                                 "make": "3d персонаж, 3d окружение, моделирование текстуры поверхности",
     *                                 "platforms": "для PC, Playstation 4, Nintendo, Switch/DS, Xbox One, Mobile"
     *                             }
     *                         ]
     *                     },
     *                     "images": [
     *                         "images/GmVFm47VrrBsQYB5hZ5o7W0RYGbd66YgDePzuU0F.png",
     *                         "images/eY89kXErwWeFNGq4yGCrcQ3wmtngq8GVP5swbiX0.jpeg"
     *                     ]
     *                 }
     *         }
     *     }
     */		
	public function team()
    {
        if ($team = Team::select('id','sort','image') 
			->with('people','images')
			->get()
			->toArray()) {		
			
			$i = 0;
			foreach ($team As $item) {
				$team[$i]['people'] = collect($item['people'])->groupby('language');
				$team[$i]['EN'] = $team[$i]['people']['EN'][0];
				$team[$i]['RU'] = $team[$i]['people']['RU'][0];
				$team[$i]['images'] = collect($item['images'])->pluck('url');
				$team[$i]['image'] = ($item['image']) ? asset('storage/' . $item['image']) : null;
				unset($team[$i]['people']);
				$i++;
			}
			
			return $this->payload($team);
		} else {
			return $this->errorResponse("Нет ни одного члена команды", 404);
		}
    }
	
		/**
     * @api {get} /game/{slug} Данные игры
     * @apiGroup Data
	 * @apiVersion 0.2.2
     * @apiName Данные указанной игры
     * @apiParam {slug} string служебное имя игры, всего две [mechattacks,boomboom]
     *
     * @apiSuccessExample Success-Response
     *     HTTP/1.1 200 OK
     *     {
     *         "status": 200,	
     *         "payload": {
     *         "id": 1,
     *         "slug": "mechattacks",
     *         "name": "mechattacks",
     *         "items": {
     *         "RU": [
     *         {
     *             "id": 1,
     *              "game_id": 1,
     *              "language": "RU",
     *              "type": "string",
     *              "slug": "title",
     *              "value": "Это строка"
     *          },
     *          {
     *              "id": 15,
     *              "game_id": 1,
     *              "language": "RU",
     *              "type": "image",
     *              "slug": "dcdcdc",
     *              "value": "images/AiRg11xQlxwmL2yDCIXkwUTTMiWIRrrXhOSHLPnC.png"
     *          }
     *         ],
     *         "EN": [
     *          {
     *              "id": 5,
     *              "game_id": 1,
     *              "language": "EN",
     *              "type": "string",
     *              "slug": "sdsfsdfsd",
     *              "value": "fsfsdfsdf"
     *          },
     *          {
     *              "id": 14,
     *              "game_id": 1,
     *              "language": "EN",
     *              "type": "array",
     *              "slug": "fff",
     *              "value": [
     *                  "ggg",
     *                  "hhh",
     *                  "jjj"
     *              ]
     *          }
     *          ]
     *          },
     *          "sections": [
     *          {
     *          "id": 1,
     *          "game_id": 1,
     *          "slug": "weapons",
     *          "name": "Оружие",
     *          "title": "оружие",
     *          "items": {
     *          "RU": [
     *                  {
     *                      "id": 1,
     *                      "game_section_id": 1,
     *                      "language": "RU",
     *                      "slug": "",
     *                      "title": "удар молнии",
     *                      "text": "<p>Удар молнии, также известный как «Молниеносный пистолет» или просто «LG», излучает постоянный луч электричества. Он может нанести невероятный урон, если остается на цели. Оружие мгновенно поразит цель, на которую оно направлено, что делает его более легким в использовании, чем гвоздодер или супер<br></p>",
     *                      "image": null
     *                  }
     *              ],
     *              "EN": [
     *                  {
     *                      "id": 3,
     *                      "game_section_id": 1,
     *                      "language": "EN",
     *                      "slug": "",
     *                      "title": "44444",
     *                      "text": "<p>4444<br></p>",
     *                      "image": null
     *                  },
     *                  {
     *                      "id": 4,
     *                      "game_section_id": 1,
     *                      "language": "EN",
     *                      "slug": "",
     *                      "title": "wwww",
     *                      "text": "",
     *                      "image": "images/gz2eVh1T3wCPk1uzwjxvU9hDdThtVV82v2u18g9m.jpeg"
     *                  },
     *                  {
     *                      "id": 6,
     *                      "game_section_id": 1,
     *                      "language": "EN",
     *                      "slug": "",
     *                      "title": "С картинкой",
     *                      "text": "<p>всвввв<br></p>",
     *                      "image": null
     *                  }
     *              ]
     * 			}
     * 		},      
     *      ]
     * 	 }
     * }
     */		
	public function game($slug)
    {	
		$games = Game::where('slug',$slug);
        if ($games->exists()) {			
			return $this->payload($this->game_info($games));
		} else {
			return $this->errorResponse("Не найдена такая игра", 404);
		}
    }
	
	private function game_info($query) {
		$languages 	= ['EN','RU'];
		$roles 		= Role::all();
				
		
		$games = $query->with('items')
			->with(['sections' => function ($q) {
				$q->with('items');
			}])
			->with(['mech' => function ($q) {
				$q->with('rows');
			}])
			->with(['heroes' => function ($q) {
				$q->orderBy('orders','asc')->with('weapons');
			}])
			->first()
			->toArray();
		
			$i = 0;		
			foreach ($games['items'] As $item) {					
				if ($item['type'] == 'array') {
					$games['items'][$i]['value'] = explode("|",$item['value']);
				}
				if ($item['type'] == 'image') {
					$games['items'][$i]['value'] = ($games['items'][$i]['value']) ? asset('storage/' . $games['items'][$i]['value']) : null;
				}
				
				// Роли
				if ($item['slug'] == 'roles') {
					$tmp = $games['items'][$i];
					$out = $roles->where('language',$tmp['language'])
						->where('game_id',$tmp['game_id'])
						->whereIn('key',$tmp['value']);
					$out = $out->map(function ($item) {						
						return $item->only('key','title');
					});
						
					$games['items'][$i]['value'] = array_values($out->toArray());					
				}
				
				$i++;
			}
			
			$i = 0;
			foreach ($games['heroes'] As $item) {
				$games['heroes'][$i]['image'] = ($games['heroes'][$i]['image']) ? asset('storage/' . $games['heroes'][$i]['image']) : null;
				$games['heroes'][$i]['id'] = $games['heroes'][$i]['slug'];
				if (isset($games['heroes'][$i]['weapons'])) {
					$j = 0;
					foreach ($games['heroes'][$i]['weapons'] AS $e) {
						$games['heroes'][$i]['weapons'][$j]['image'] = ($e['image']) ? asset('storage/' . $e['image']) : null;
						$j++;
					}
				}
								
				
				$i++;
			}
			
			// Тупо в лоб сортируем
			$heroes = $games['heroes'];
			uasort($heroes, function ($a, $b) {
				return strcmp($a['orders'], $b['orders']);
			});
			$games['heroes'] = array_values($heroes);
			//dd(array_values($games['heroes']));
			
			foreach ($languages As $language) {				
				$v = collect($games['items'])
					->where('language',$language)
					->values();					
				foreach ($v AS $item) {
					$games[$language][$item['slug']] = $item['value'];
				}
				
				// оружие, разработки
				$v = collect($games['sections']);
				foreach ($v AS $item) {
					
					$w = collect($item['items'])
						->where('language',$language)
						->values()
						->transform(function ($item, $key) {
						return $item = [
							'title' => $item['title'],
							'text' => $item['text'],
							'image' => ($item['image']) ? asset('storage/' . $item['image']) : null
						];
					});					
					$games[$language][$item['slug']] = $w;
				}

				// Мехи
				$v = collect($games['mech'])
					->where('language',$language)
					->values();								
				
				
				$i = 0;
				foreach ($v AS $item) {						
					$w = collect($item['rows'])				
						->transform(function ($item, $key) {							
						$z = [
							'type' => $item['type'],
							'columns' => explode('|',$item['columns'])							
						];
						$item = $z;
					return $item;
					});
										
					$item['image'] = ($item['image']) ? asset('storage/' . $item['image']) : null;
					$item['id'] = $item['slug'];
					$games[$language]['robots'][$i] = $item;					
					$games[$language]['robots'][$i]['table'] = $w;
					unset($games[$language]['robots'][$i]['game_id'],$games[$language]['robots'][$i]['language'],$games[$language]['robots'][$i]['rows'],$games[$language]['robots'][$i]['max_col']);
					$i++;
				}								
				
			}

			
			unset($games['items']);
			unset($games['sections']);
			unset($games['mech']);
			return $games;
		}
		
}
