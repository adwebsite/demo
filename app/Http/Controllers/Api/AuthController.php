<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponses;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordReset;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Newsletter;

class AuthController extends Controller
{
    use ApiResponses;

    /**
     * @api /* Авторизированные запросы
     * @apiGroup Authentication
     * @apiName AuthenticatedRequests
     *
     * @apiDescription Для выполнения запросов от имени авторизированного пользователя добавьте заголовок "Authorization" со значением "Bearer *ТОКЕН*" к запросу. Запросы шлем на http://api.mack-software.com
     *
     */

    /**
     * @api {post} /auth/register Регистрация
     * @apiGroup Authentication
     * @apiName Регистрация
     *
     * @apiParam {String} name Логин	 
     * @apiParam {String} email E-mail адрес
     * @apiParam {String} password Пароль
     * @apiParam {String} password_confirmation Подтверждение пароля
	 * @apiParam {String} lang Язык
     *
     * @apiSuccessExample Success-Response
     *     HTTP/1.1 201 OK
     *     {
     *         "status": 201,
     *         "payload": {
     *             "name": "Vasyan",
     *             "email": "user2@example.com",             
	 *             "lang": "en",
     *             "updated_at": "2018-05-30 10:32:00",
     *             "created_at": "2018-05-30 10:32:00",
     *             "id": 3
     *         }
     *     }
     */
    public function register()
    {		
		$validate = $this->validateRegistration(request()->all());
		if ($validate->passes()) {
			$user = User::create([
				'lang' => request('lang'),
				'name' => request('name'),
				'email' => request('email'),
				'password' => Hash::make(request('password'))				
			]);
			
			Newsletter::subscribe(request('email'), ['FNAME' => request('name'),'LANG' => request('lang')]);

			return $this->payload($user, 201);
			
		} else {
			$error = $validate->messages()->toArray();
			$message_ru = (isset($error['email']) && $error['email']) == "Такое значение уже занято." ? "Пользователь с такой почтой уже существует" : "Ошибка валидации";
			$message_en = (isset($error['email']) && $error['email']) == "Такое значение уже занято." ? "This email already exist." : "Error validation";
			return response()->json(['userMessage' => [
				'RU' => $message_ru,
				'EN' => $message_en]],
				422, [], JSON_UNESCAPED_UNICODE);
		//$this->errorResponse($validate->messages(), 422);
			
		}
    }

    /**
     * @api {post} /auth/login Вход
     * @apiGroup Authentication
     * @apiName Вход
     *
     * @apiParam {String} email E-mail адрес
     * @apiParam {String} password Пароль
     *
     * @apiSuccessExample Success-Response
     *     HTTP/1.1 200 OK
     *     {
     *         "status": 200,
     *         "payload": {
     *             "token_type": "Bearer",
     *             "expires_in": 31536000,
     *             "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjRhYWM4N2E3Y2UzNzU2ZThkYmI2ODI5Y2QwYTNmOTdkNjE3Mzc3NGM4OGM4N2ExZTRhNWMzODk1MmQ2NGY2OTExMGJiYmYzYWFmMjQ5N2RlIn0.eyJhdWQiOiIxIiwianRpIjoiNGFhYzg3YTdjZTM3NTZlOGRiYjY4MjljZDBhM2Y5N2Q2MTczNzc0Yzg4Yzg3YTFlNGE1YzM4OTUyZDY0ZjY5MTEwYmJiZjNhYWYyNDk3ZGUiLCJpYXQiOjE1Mjc2ODIyMDksIm5iZiI6MTUyNzY4MjIwOSwiZXhwIjoxNTU5MjE4MjA5LCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.O9P1cxlNIMXwWVqa0dwlIgyslvvCu7E1xiaOW3b9LBn0t5fRR71_8h2eG3eVNb2UmS9OG0aYBw4INjezwzSmXf7M1dr6-uWU4J6F31yEv8RY52AMANQ3fuLS6ReTcIDwO2rlmmanp5YQ7VD8VUKF90Q5RERZDReoB07UMp-2D-QXsjwjsUbei64ZNHXbDLB2LGniQk5QQRQ7-aYdKI_WxPXihORLPQpyKpRdhEIjq4-oLuczJoNBg8iweE2e7wYcSx144bsuHR40r-hdFW99aq6mkKZx5jn2wCI6eoaS_Q1Gs1EwsUM4VAnd68Q8EkflvyDQdV3nGL728JBr3BNIyVPkixqhz7Yby7YN3vvSSvgikhO9z3ddtvr4y2dsBdU_gPvV2os7YOOVZdhZjTPl16Ibc2DJ5eqK-27zXyEAgrF4keZP3QZnoGvmfnLRGusrCnvXdmhDByUI-qsygedRSPKCr02EReIljXFnMGfAPiEpax5CKk1iODq8EG3QRa5ln9H4xofo1_OcI3mIagW149pCUbFerxijE4HMgLXy5tBsU0Gkzt9Ys3Ky5aAzMppGKk2Dl6xq2bTlocjnoLvdzte9p5weTgH3O5PKhI4T-SNu5aSq0YWSCAbXnFwoy4xkpb014sZAJEr51YsTve0GsZdRXIY_BTFUC-Pvd1wL36M",
     *             "refresh_token": "def50200054dfb11caf6598cd06bb4224a626a5bd4af3ea897253f1a9f5680d100b10859011e48bf681598f4f9da023056f37554ed33225ddfc9ffe0fe517a7a725f6e701e0217aa2f7a64920bce7d24e441493555a84cd51338d23b99dcb60d4d68808e0736167246f34cf8e47e0241be2915a66a8582f2e9c2be91b79092b09696b6817eb73a5ce5b9c4b71c9640efe7d0d75a163b7a5344c9b3dd66da05e628c4a97caab9c2e82e941034e4933fc17d076bb189ed5540e3e5fc05e15b8b87397e8ad9ce4fc475203cf763f9a82e518d66fa4e4908719b860a36b89f778399c67135a943213408a704ed7701e1779e2fb7d85bda295f548fd14adf6ca19f75a9c5a8962c2dd15000c1e29403cd5f49c1b4411c6aab544edf08d74409e92b9fdbf5c244661ee9529252416104fccdd158b383df9f90ebc8fd44c6ddc7ce339653b17518eac33a6cc0723cc804393bf1ed42467760e14e3a6941b2ad8fc9e67318"
     *         }
     *     }
     */
    public function login()
    {
		
		$credentials = request(['email', 'password']);
        
        // Check if the user's email is verified
        $user = User::where('email', request('email'))->first();

        $client = DB::table('oauth_clients')
            ->where('password_client', true)
            ->where('revoked', false)
            ->first();

        if (!$client) {
            return $this->errorResponse('Клиенты для Laravel Passport не сгенерированы.', 500);
        }

        $data = [
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => request('email'),
            'password' => request('password'),
			'scope'    => '*'
        ];

        $request = Request::create('/oauth/token', 'POST', $data);

        $response = app()->handle($request);
		$status = $response->getStatusCode();
		
		if ($status > 202) {
			$error = json_decode($response->getContent())->error;
			//invalid_credentials
			$message_ru = ($error == "invalid_credentials") ? "Неверный логин/пароль" : "Ошибка валидации";
			$message_en = ($error == "invalid_credentials") ? "Incorrect login/password" : "Error validation";
			
			return response()->json(['userMessage' => [
				'RU' => $message_ru,
				'EN' => $message_en]],
				422, [], JSON_UNESCAPED_UNICODE);
		} else {
			return $this->payload(json_decode($response->getContent(),true));
		}
    }
	
	
    /**
     * @api {post} /auth/social Авторизация/Регистрация через социалки
     * @apiGroup Authentication
	 * @apiVersion 0.3.1
     * @apiName Авторизация/Регистрация через социалки
	 * @apiDescription Передаются данные от соцсетей после авторизации/регистрации на сайте.
     *
     * @apiParam {String} social_provider Идентификатор социальной сети [vk,fb]
	 * @apiParam {String} social_id Идентификатор пользователя в социальной сети
	 * @apiParam {String} [name] Имя пользователя социалки
	 * @apiParam {String} lang Язык
     * @apiParam {String} email E-mail адрес
     *
     * @apiSuccessExample Success-Response Register
     *     HTTP/1.1 201 OK
     *     {
     *         "status": 201,
     *         "payload": {
	 *             "social_provider": "vk",
     *             "social_id": "123456789099876565444",
     *             "name": "Vasyan",
	 *             "lang": "ru",
     *             "email": "user2@example.com",            
     *             "updated_at": "2018-05-30 10:32:00",
     *             "created_at": "2018-05-30 10:32:00",
     *             "id": 3,
	 *             "token" : "....... token"
     *         }
     *     }
	 *
     * @apiSuccessExample Success-Response Login
     *     HTTP/1.1 201 OK
     *     {
     *         "status": 201,
     *         "token": "....... token"
     *         
     *     }
     */
    public function social()
    {		
		$validate = $this->validateSocial(request()->all());
		if ($validate->passes()) {
			$user = User::where("social_provider",request('social_provider'))
				->where("social_id",request('social_id'));
			if ($user->exists()) {
				// Авторизация
				
				$token = $user->first()->createToken('')->accessToken;
				return $this->payload(["token"=>$token]);						
			} else {
				// Регистрация
				$name = (!empty(request('name'))) ? request('name') : 'user_'.request('social_provider')."_".request('social_id');
				
				$user = User::create([
					'social_provider' => request('social_provider'),
					'social_id' => request('social_id'),
					'name' => $name,
					'lang' => request('lang'),
					'email' => !empty(request('email')) ? request('email') : '',
					//'password' => Hash::make(request('password'))				
				]);
				
				// Если есть email - в mailshimp его
				if (!empty(request('email'))) {
					Newsletter::subscribe(request('email'), ['FNAME' => $name,'LANG' => request('lang')]);
				}
				
				$token = $user->first()->createToken('')->accessToken;
				$user->token = $token;

				return $this->payload($user, 201);			
			}
		} else {
			$error = $validate->messages()->toArray();			
			$message_ru = isset($error['email_uniq']) ?  $error['email_uniq'][0] : "Ошибка валидации";
			$message_en = isset($error['email_uniq']) ?  "This email already exist for this social account." : "Error validation";
			return response()->json(['userMessage' => [
				'RU' => $message_ru,
				'EN' => $message_en]],
				422, [], JSON_UNESCAPED_UNICODE);			
		}
    }

    /**
     * @api {post} /auth/password/reset Запрос сброса пароля
     * @apiGroup Authentication
     * @apiName Запрос сброса пароля
     *
     * @apiParam {String} email E-mail адрес
     *
     * @apiSuccessExample Success-Response
     *     HTTP/1.1 201 OK
     *     {
     *         "status": 201,
     *         "payload": "Ссылка для сброса пароля была отправлена на указанный E-mail."
     *     }
     */
    public function sendPasswordResetLink()
    {
		$validator = Validator::make(request()->all(), [
            'email' => 'required|email|string|max:255'
		]);
        
		if ($validator->passes()) {

			if ($user = User::where('email', request('email'))->first()) {
		
				$token = app('auth.password.broker')->createToken($user);
		
				Mail::to($user->email)->send(new PasswordReset($token));
		
				return $this->payload(['email'=>'Ссылка для сброса пароля была отправлена на указанный E-mail.']);
			} else {
				
				return response()->json(['userMessage' => [
					'RU' => 'Нет пользователя с указанным e-mail',
					'EN' => 'No exist this email']],
				404, [], JSON_UNESCAPED_UNICODE);				
			}
		} else {
			
			return response()->json(['userMessage' => [
					'RU' => 'Ошибка валидации',
					'EN' => 'Error validation']],
				422, [], JSON_UNESCAPED_UNICODE);
			
		}
    }

    public function validateRegistration($data)
    {		
        $validator = Validator::make($data, [
			'lang' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
			'password_confirmation' => 'required|string|min:6',            
        ]);
        
        return $validator;
    }
	
	public function validateSocial($data)
    {		
        $validator = Validator::make($data, [
			'lang' => 'required|string|max:255',
            'social_provider' => 'required|string|max:255',
			'social_id' => 'required|string|max:255',
            'email' => 'sometimes|string|email|max:255|nullable',
        ]);
		
		$validator->after(function ($validator) use ($data) {
			if (isset($data['user']) && User::where('social_provider',$data['social_provider'])
				->where('email',$data['email'])
				->where('social_id','!=',$data['social_id'])
				->exists()) {
			        $validator->errors()->add(
                        'email_uniq',
                        'Уже есть такой e-mail для указанной соцсети'
                    );
            }            
        });
        
        return $validator;
    }
}
