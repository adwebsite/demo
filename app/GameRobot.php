<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Voerro\FileUploader\FileUploader;
use App\RobotTable;

class GameRobot extends Model
{
    protected $fillable = [
		'game_id',
		'language',
        'slug',
		'name',
		'image',
		'role',
		'description',
		'tableTitle',
		'max_col'
    ];
	
	public $timestamps = false;
	
	protected static function boot()
    {
        parent::boot();
		
		// Если это изображение - то удаление
		static::deleting(function ($model) {			
			FileUploader::delete($model->image);			
		});		
    }
	
	public function role2()
    {		
        return $this->hasMany('App\Role', 'key','role')			
			->where('visible',1);
    }
    
	public function rows()
    {
        return $this->hasMany('App\RobotTable', 'robot_id')->orderBy('id','ASC');
    }	
	
}