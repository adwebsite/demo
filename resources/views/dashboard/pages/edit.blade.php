@extends('layouts.dashboard')

@section('header-css')
	<link rel="stylesheet" href="{{ asset('/css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/plugins/bootstrap-fileinput/fileinput.min.css') }}">
@endsection

@section('page-title')
Редактирование страницы
@endsection

@section('nav-layout')
<li>Страницы</li>
<li class="active"><b>{{ $page->name }} ({{ $page->slug }})</b></li>
@endsection

@section('main')

@if (!empty(session('message'))) <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('message') }} </div> @endif 
<!-- основные поля -->
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">{{ $page->name }} ({{ $page->slug }})</h3>
	</div>
	<!-- /.box-header -->
	<!-- form start -->
	<form class="form-horizontal" action="{{ route('dashboard.pages.update', $page) }}" method="POST" id="page-form" role="form" enctype="multipart/form-data" >
		@csrf
		<div class="box-body">
			@if (!empty($general_items))
				@foreach ($general_items as $items)
				@endforeach
			@endif
				
		</div>
		<!-- основные -->
		
		@if (!empty($children_page))
			@foreach ($children_page as $items)			
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Раздел: {{ $items->name }} ({{ $items->slug }})</h3>
						<div class="box-body">
							<div class="container">
								@foreach($items->children->groupBy('group_item') as $line)								
									@foreach ($line as $item)
										<div class="form-group row">
											<label class="col-sm-3 control-label" for="value[{{ $item->id }}]">{{ $item->slug }}</label>
											@if ($item->type == 'string')
											<div class="input-group col-sm-9">
												<input type="text" id="value[{{ $item->id }}]" name="value[{{ $item->id }}]" class="form-control" value="{{ $item->value }}">
												@if ($item->language) 
													<span class="input-group-addon" title="Язык">{{ $item->language }}</span>
												@endif
											</div>
											@elseif ($item->type == 'link')
											
											<div class="input-group col-sm-9">
												<input type="text" id="value[{{ $item->id }}]" name="value[{{ $item->id }}]" class="form-control" value="{{ $item->value }}">
												@if ($item->language) 
													<span class="input-group-addon" title="Язык">{{ $item->language }}</span>
												@endif
											</div>
											</div>
											<div class="form-group row">
											<label class="col-sm-3 control-label">href</label>
											<div class="input-group col-sm-9">
												<input type="text" id="value_[{{ $item->id }}]" name="href[{{ $item->id }}]" class="form-control" value="{{ $item->href }}">
											</div>
											
											@elseif ($item->type == "image")
												<div class="input-group col-sm-8">
													<input class="image" name="value[{{ $item->id }}]" type="file" data-initial-preview="{{ (!empty($item->value)) ? "<img src='/storage/$item->value' class='file-preview-image kv-preview-data'>" : "" }} ">
												</div>
											@elseif ($item->type == "imagelink")
												<div class="input-group col-sm-8">
													<input class="image" name="value[{{ $item->id }}]" type="file" data-initial-preview="{{ (!empty($item->value)) ? "<img src='/storage/$item->value' class='file-preview-image kv-preview-data'>" : "" }} ">
												</div>
											</div>
											<div class="form-group row">
											<label class="col-sm-3 control-label">href</label>
											<div class="input-group col-sm-9">
												<input type="text" id="value_[{{ $item->id }}]" name="href[{{ $item->id }}]" class="form-control" value="{{ $item->href }}">
											</div>
											@endif
										</div>
									@endforeach
									<hr/>
								@endforeach
							</div>	
						</div>
					</div>
				</div>
			@endforeach
		@endif
			
		<div class="box-footer">			
			<input type="submit" class="btn btn-success" value="Сохранить" >
		</div>
	</form>
</div>
	<!-- /.box -->


@endsection

@section('footer-js')

<script src="{{ asset('/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('/js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('/js/plugins/bootstrap-fileinput/locales/ru.js') }}"></script>
<script>
	$(".alert-success").delay(1000).fadeOut(1500);

	$(".image").fileinput({
		language: "ru",
		allowedFileExtensions: ["jpg", "png", "gif"],
		allowedFileTypes: ['image'],		
		showUpload: false,
		showCaption: false, 
		dropZoneEnabled: true,
		overwriteInitial: true,
		autoReplace: true,
		uploadAsync: false,
		maxFileCount  : 1,
		
	});
</script>

@endsection