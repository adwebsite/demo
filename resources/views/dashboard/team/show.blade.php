@extends('layouts.dashboard')

@section('header-css')
	<link rel="stylesheet" href="{{ asset('/css/plugins/bootstrap-table/bootstrap-table.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/plugins/iCheck/all.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/plugins/ekko-lightbox.css') }}">
@endsection

@section('page-title')
Редактирование команды
@endsection

@section('nav-layout')
<li>Страницы</li>
<li class="active"><b>Команда (team)</b></li>
@endsection

@section('main')

@if (!empty(session('message'))) <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('message') }} </div> @endif 
<!-- основные поля -->
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Команда (team)</h3>
	</div>
	<!-- /.box-header -->
	
	<div class="panel panel-default">
		<div class="panel-body">
			<a href="/dashboard/team/create" id="create_btn">
				<button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-asterisk"></i> <span id="create_btn">Добавить нового участника</span></button>
			</a>
		</div>
	</div>
	
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="box-body">				
				<table id="table"
						data-url="/dashboard/team"
						data-row-style="rowStyle"
						data-striped="true"
						data-toggle="table" 
						data-locale="ru-RU"										
						data-side-pagination="server"
						data-pagination="true"
						data-search="false"
						data-page-list="[15, 30, 50, 100, 200]"
						>
					<thead>
						<tr>							
							<th data-field="id" data-sortable="true">ID</th>
							<th data-field="image" data-formatter="imageFormatter">Изображение</th>
							<th data-field="name" data-sortable="true" data-formatter="langFormatter">Имя</th>
							<th data-field="position" data-sortable="true" data-formatter="langFormatter">Должность</th>
							<th data-field="make" data-sortable="true" data-formatter="langFormatter">Сфера деятельности</th>							
							<th data-formatter="actionFormatter" data-events="actionEvents" data-class='actions-row'>Действия</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- /.box -->


@endsection

@section('footer-js')
<script src="{{ asset('/js/plugins/bootstrap-table/bootstrap-table.min.js') }}"></script>
<script src="{{ asset('/js/plugins/bootstrap-table/locale/bootstrap-table-ru-RU.min.js') }}"></script>
<script src="{{ asset('/js/plugins/ekko-lightbox.min.js') }}"></script>

<script src="{{ asset('/js/app/confirm.js') }}"></script>
<script src="{{ asset('/js/app/team_list.js') }}"></script>

@endsection