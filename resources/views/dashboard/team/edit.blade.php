@extends('layouts.dashboard')

@section('header-css')	
	<link rel="stylesheet" href="{{ asset('/css/plugins/bootstrap-fileinput/fileinput.min.css') }}">
@endsection

@section('page-title')
{{ isset($people) ? 'Редактирование участника' : 'Добавление участника' }}
@endsection

@section('nav-layout')
<li>Страницы</li> <li><a href="/dashboard/pages/team">Команда</a></li>
<li class="active"><b>{{ isset($team) ? 'Редактирование участника' : 'Добавление участника' }}</b></li>
@endsection

@section('main')

@if (!empty(session('message'))) <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('message') }} </div> @endif 
<!-- основные поля -->
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">{{ isset($people) ? 'Редактирование участника' : 'Добавление участника' }}</h3>
	</div>
	<!-- /.box-header -->
	<!-- form start -->
	<form class="form-horizontal" action='{{ isset($team) ? "/dashboard/team/$team->id" : "/dashboard/team" }}' method="POST" id="page-form" role="form" enctype="multipart/form-data" >
		@csrf
		<div class="box-body">
			@if (!empty($general_items))
				@foreach ($general_items as $items)
				@endforeach
			@endif
				
		</div>
		<!-- основные -->
		<div class="box-body">
			<div class="container">
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="sort">Сортировка</label>
					</div>
					<div class="col-sm-2">
						<input type="text" id="sort" name="sort" class="form-control" value="{{ !empty($team) ? $team->sort : '' }}">
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="sort">Фото участника</label>
					</div>
					<div class="col-sm-6">
						<input class="image" name="image" type="file" data-initial-preview="{{ (!empty($team->image)) ? "<img src='/storage/$team->image' class='file-preview-image kv-preview-data'>" : '' }}">
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="sort">Дополнительные фото</label>
					</div>
					<div class="col-sm-6">
						<input class="images" name="images[]" type="file" multiple>
						{{--	data-initial-preview="{{ (!empty($team->image)) ? "<img src='/storage/$team->image' class='file-preview-image kv-preview-data'>" : "" }}"> --}}
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="EN[name]">name</label>
					</div>
					<div class="input-group col-sm-6">
						<input type="text" id="EN[name]" name="EN[name]" class="form-control" value="{{ !empty($people['EN']->name) ? $people['EN']->name : '' }}">						
						<span class="input-group-addon" title="Язык">EN</span>						
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="RU[name]">name</label>
					</div>
					<div class="input-group col-sm-6">
						<input type="text" id="RU[name]" name="RU[name]" class="form-control" value="{{ !empty($people['RU']->name) ? $people['RU']->name : '' }}">						
						<span class="input-group-addon" title="Язык">RU</span>						
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="EN[position]">position</label>
					</div>
					<div class="input-group col-sm-6">
						<input type="text" id="EN[position]" name="EN[position]" class="form-control" value="{{ !empty($people['EN']->position ) ? $people['EN']->position : '' }}">						
						<span class="input-group-addon" title="Язык">EN</span>						
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="RU[position]">position</label>
					</div>
					<div class="input-group col-sm-6">
						<input type="text" id="RU[position]" name="RU[position]" class="form-control" value="{{ !empty($people['RU']->position) ? $people['RU']->position : '' }}">						
						<span class="input-group-addon" title="Язык">RU</span>						
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="EN[make]">make</label>
					</div>
					<div class="input-group col-sm-6">
						<input type="text" id="EN[make]" name="EN[make]" class="form-control" value="{{ !empty($people['EN']->make) ? $people['EN']->make : '' }}">						
						<span class="input-group-addon" title="Язык">EN</span>						
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="RU[make]">make</label>
					</div>
					<div class="input-group col-sm-6">
						<input type="text" id="RU[make]" name="RU[make]" class="form-control" value="{{ !empty($people['RU']->make) ? $people['RU']->make : '' }}">						
						<span class="input-group-addon" title="Язык">RU</span>						
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="EN[platforms]">platforms</label>
					</div>
					<div class="input-group col-sm-6">
						<input type="text" id="EN[platforms]" name="EN[platforms]" class="form-control" value="{{ !empty($people['EN']->platforms) ? $people['EN']->platforms : '' }}">						
						<span class="input-group-addon" title="Язык">EN</span>						
					</div>
				</div>
				
				<div class="form-group row">
					<div class="col-sm-2">
						<label class="control-label" for="RU[platforms]">platforms</label>
					</div>
					<div class="input-group col-sm-6">
						<input type="text" id="RU[platforms]" name="RU[platforms]" class="form-control" value="{{ !empty($people['RU']->platforms) ? $people['RU']->platforms : '' }}">						
						<span class="input-group-addon" title="Язык">RU</span>						
					</div>
				</div>
				
			</div>
		</div>
			
							
					
		<div class="box-footer">
			<a class="btn btn-warning" href="/dashboard/team">Отмена</a>
			<input type="submit" class="btn btn-success" value="Сохранить" >
		</div>
	</form>
</div>
	<!-- /.box -->


@endsection

@section('footer-js')

<script src="{{ asset('/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('/js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('/js/plugins/bootstrap-fileinput/locales/ru.js') }}"></script>
<script>

	csrf = $("meta[name=csrf-token]").attr("content");
	
	$(".image").fileinput({
		language: "ru",
		allowedFileExtensions: ["jpg", "png", "gif"],
		allowedFileTypes: ['image'],		
		showUpload: false,
		showCaption: false, 
		dropZoneEnabled: true,
		overwriteInitial: true,
		autoReplace: true,
		uploadAsync: false,
		maxFileCount  : 1,
		deleteUrl: "/dashboard/image/delete?_token="+csrf,	
		initialPreviewConfig: [{'key' : {{ isset($team) ? $team->id : 0 }} }],
	});
	
	images = [
	@if (!empty($images))
	@foreach ($images AS $item)
	"<img src='/storage/{{ $item->image }}' class='file-preview-image kv-preview-data'>",
	@endforeach
	@endif
	];
	
	keys = [
	@if (!empty($images))
	@foreach ($images AS $item)
	{'key' : {{ $item->id }} },
	@endforeach
	@endif
	];
	
		
	$(".images").fileinput({
		language: "ru",
		allowedFileExtensions: ["jpg", "png", "gif"],
		allowedFileTypes: ['image'],		
		showRemove: false,
        showUpload: false,
		overwriteInitial: false,		
		showCaption: false,
		//initialPreviewAsData: false,
		uploadAsync: false,
		maxFileCount  : 5,
		initialPreviewFileType: 'image',
		theme: 'explorer-fa',
		initialPreview: images,
		deleteUrl: "/dashboard/images/delete?_token="+csrf,
		initialPreviewConfig: keys,
	});
</script>

@endsection