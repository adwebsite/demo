@extends('layouts.dashboard')

@section('header-css')
	<link rel="stylesheet" href="{{ asset('/css/plugins/bootstrap-table/bootstrap-table.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/plugins/iCheck/all.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/plugins/ekko-lightbox.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/plugins/bootstrap-fileinput/fileinput.min.css') }}">
@endsection

@section('page-title')
Игра {{ $game->name }}
@endsection

@section('nav-layout')
<li>Игры</li>
<li class="active"><b>Игра {{ $game->name }}</b></li>
@endsection

@section('main')

@if (!empty(session('message'))) <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('message') }} </div> @endif 
<!-- основные поля -->
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Игра {{ $game->name }}</h3>
	</div>
	<!-- /.box-header -->
	
	<div class="panel panel-default">
		<div class="panel-body">			
			<div class="btn-group">
				<button type="button" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-asterisk"></i> <span id="create_btn">Добавить новый элемент</span></button>
				<button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
					<span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
				</button>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#" class="js-add-item" data-type="string" data-game="{{ $game->id }}">Строка</a></li>
					<li><a href="#" class="js-add-item" data-type="text" data-game="{{ $game->id }}">Текст</a></li>
					<li><a href="#" class="js-add-item" data-type="image" data-game="{{ $game->id }}">Изображение</a></li>
					<li><a href="#" class="js-add-item" data-type="array" data-game="{{ $game->id }}">Массив значений</a></li>
					<li class="divider"></li>
					<li><a href="#">Separated link</a></li>
				</ul>
			</div>
			
		</div>
	</div>
	
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="box-body">				
				<table id="table"
						data-url="/dashboard/game-item/{{ $game->id }}"
						data-row-style="rowStyle"
						data-striped="true"
						data-toggle="table" 
						data-locale="ru-RU"								
						data-side-pagination="server"
						data-pagination="true"
						data-search="false"
						data-sort-name="language"
						data-sort-order="asc"
						data-page-list="[15, 30, 50, 100, 200]"
						>
					<thead>
						<tr>							
							<th data-field="language" data-sortable="true">Язык</th>
							<th data-field="type" data-formatter="TypeFormatter" data-sortable="true">Тип</th>
							<th data-field="slug" data-sortable="true">Служебное имя</th>
							<th data-field="value" data-formatter="valueFormatter">Значение</th>							
							<th data-formatter="actionFormatter" data-events="actionEvents" data-class='actions-row'>Действия</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- /.box -->


@endsection

@section('footer-js')
<script src="{{ asset('/js/plugins/bootstrap-table/bootstrap-table.min.js') }}"></script>
<script src="{{ asset('/js/plugins/bootstrap-table/locale/bootstrap-table-ru-RU.min.js') }}"></script>
<script src="{{ asset('/js/plugins/ekko-lightbox.min.js') }}"></script>
<script src="{{ asset('/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('/js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('/js/plugins/bootstrap-fileinput/locales/ru.js') }}"></script>


<script src="{{ asset('/js/app/confirm.js') }}"></script>
<script src="{{ asset('/js/app/game_list.js') }}"></script>

@endsection