<div class="form-group">
<form name="items" id="form-items" role="form" enctype="multipart/form-data">
@switch($type)
@case('string')
<h4>Строка</h4>
<label class="control-label" for="slug">Служебное имя</label>
<input type="text" id="slug" name="slug" class="form-control" value="{{ !empty($edit) ? $edit->slug : ''}}">

<label class="control-label" for="value">Значение</label>
<input type="text" id="value" name="value" class="form-control" value="{{ !empty($edit) ? $edit->value : ''}}">

<label class="control-label">Язык</label>
<select class="form-group select2" name="language" id="language" style="width:100%">
	@foreach ($languages as $items)
		<option value="{{ $items->slug }}" {{ ($edit && ($items->slug == $edit->language)) ? 'selected' : '' }}> {{ $items->title }}</option>
	@endforeach
</select>
<script>
	$('.select2').select2();
</script>
@break

@case('text')
<h4>Текст</h4>
<label class="control-label" for="slug">Служебное имя</label>
<input type="text" id="slug" name="slug" class="form-control" value="{{ !empty($edit) ? $edit->slug : ''}}">

<label class="control-label" for="value">Значение</label>
<textarea class="textarea" id="value" name="value" placeholder="Описание" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
{{ !empty($edit) ? $edit->value : '' }}
</textarea>

<label class="control-label">Язык</label>
<select class="form-group select2" name="language" id="language" style="width:100%">
	@foreach ($languages as $items)
		<option value="{{ $items->slug }}" {{ ($edit && ($items->slug == $edit->language)) ? 'selected' : '' }}> {{ $items->title }}</option>
	@endforeach
</select>
<script>
	$('.textarea').wysihtml5();
	$('.select2').select2();
</script>
@break

@case('image')
<h4>Изображение</h4>
<label class="control-label" for="slug">Служебное имя</label>
<input type="text" id="slug" name="slug" class="form-control" value="{{ !empty($edit) ? $edit->slug : ''}}">

<label class="control-label" for="image">Изображение</label>
<input id="image" name="image" type="file">


<label class="control-label">Язык</label>
<select class="form-group select2" name="language" id="language" style="width:100%">
	@foreach ($languages as $items)
		<option value="{{ $items->slug }}" {{ ($edit && ($items->slug == $edit->language)) ? 'selected' : '' }}> {{ $items->title }}</option>
	@endforeach
</select>
<script>
	var csrf = $("meta[name=csrf-token]").attr("content");
	var delete_url = "{{ !empty($edit->value) ? '/dashboard/game-item/image/'.$edit->id.'?_token=' : '' }}";
	delete_url = (delete_url!='') ? delete_url+csrf : '""';
	$('.select2').select2();
	$("#image").fileinput({
		language: "ru",
		allowedFileExtensions: ["jpg", "png", "gif"],
		allowedFileTypes: ['image'],
		//showRemove: false,
		showUpload: false,
		showCaption: false, 
		dropZoneEnabled: true,
		overwriteInitial: true,
		autoReplace: true,
		uploadAsync: false,
		maxFileCount  : 1,
		validateInitialCount : true,		
		initialPreview: '{!! (!empty($edit->value)) ? '<img src="/storage/' . $edit->value . '" class="file-preview-image kv-preview-data">' : '' !!}',
		deleteUrl: delete_url
	});
</script>
@break

@case('array')
<h4>Массив строк</h4>
<label class="control-label" for="slug">Служебное имя</label>
<input type="text" id="slug" name="slug" class="form-control" value="{{ !empty($edit) ? $edit->slug : ''}}">

<label class="control-label" for="value">Значения (разделитель Enter)</label>
<select class="form-group select2" name="value[]" id="value_array" style="width:100%" multiple="multiple">
@if (!empty($edit->value))
	@foreach (explode("|",$edit->value) as $items)<option value="{{ $items }}" selected>{{ $items }}</option>@endforeach
@endif
</select>

<label class="control-label">Язык</label>
<select class="form-group select2" name="language" id="language" style="width:100%">
	@foreach ($languages as $items)
		<option value="{{ $items->slug }}" {{ ($edit && ($items->slug == $edit->language)) ? 'selected' : '' }}> {{ $items->title }}</option>
	@endforeach
</select>
<script>
	$('.select2').select2();
	$('#value_array').select2({		
		tags: true,		
	});	
</script>
@break
@endswitch
<input type="hidden" name="type" id="form-items-type" value="{{ $type }}">
<input type="hidden" name="game_id" value="{{ $game }}">
<input type="hidden" name="edit_id" value="{{ !empty($edit) ? $edit->id : '' }}">
</form>
</div>