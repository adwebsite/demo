@extends('layouts.dashboard')

@section('header-css')
	<link rel="stylesheet" href="{{ asset('/css/plugins/bootstrap-table/bootstrap-table.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/plugins/iCheck/all.css') }}">	
@endsection

@section('page-title')
Вопросы в техподдержку
@endsection

@section('nav-layout')
<li class="active"><b>Вопросы</b></li>
@endsection

@section('main')

@if (!empty(session('message'))) <div class="alert alert-success"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('message') }} </div> @endif 
<!-- основные поля -->
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Список вопросов</h3>
	</div>
	<!-- /.box-header -->
	
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="box-body">				
				<table id="table"
						data-url="/dashboard/feedback"
						data-row-style="rowStyle"
						data-striped="true"
						data-toggle="table" 
						data-locale="ru-RU"								
						data-side-pagination="server"
						data-pagination="true"
						data-search="false"
						data-sort-name="created_at"
						data-sort-order="desc"
						data-page-list="[15, 30, 50, 100, 200]"
						>
					<thead>
						<tr>							
							<th data-field="created_at" data-sortable="true">Зарегистрирован</th>
							<th data-field="game_slug" data-sortable="true">Игра</th>
							<th data-field="questions" data-sortable="true">Вопросы</th>
							<th data-field="name" data-sortable="true">Пользователь</th>							
							<th data-field="email">E-mail</th>
							<th data-field="message">Сообщение</th>
													
							<th data-formatter="actionFormatter" data-events="actionEvents" data-class='actions-row'>Действия</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- /.box -->


@endsection

@section('footer-js')
<script src="{{ asset('/js/plugins/bootstrap-table/bootstrap-table.min.js') }}"></script>
<script src="{{ asset('/js/plugins/bootstrap-table/locale/bootstrap-table-ru-RU.min.js') }}"></script>
<script src="{{ asset('/js/plugins/ekko-lightbox.min.js') }}"></script>

<script src="{{ asset('/js/app/confirm.js') }}"></script>
<script src="{{ asset('/js/app/feedback_list.js') }}"></script>

@endsection