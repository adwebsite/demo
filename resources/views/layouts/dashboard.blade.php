@extends('layouts.app')

@section('content')
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{ route('dashboard') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">CMS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">CMS iGames</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">       
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="/images/iGame.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ auth()->user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="/images/iGame.jpg" class="img-circle" alt="User Image">

                <p>
                  {{ auth()->user()->name }}
                  <small>{{ auth()->user()->name }}</small>
                </p>
              </li>              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ route('dashboard.account.edit') }}" class="btn btn-default btn-flat">Профиль</a>
                </div>
                <div class="pull-right">
                  <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Выход</a>
                </div>
              </li>
            </ul>
          </li>          
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Страницы сайта</li>

		<li class="{{ $active_menu == 'header' ? 'active' : '' }}">
          <a href="/dashboard/pages/header">
            <i class="fa fa-graduation-cap"></i>
			<span>Шапка сайта</span>            
          </a>          
        </li>
        <li class="{{ $active_menu == 'mainPage' ? 'active' : '' }}">
          <a href="/dashboard/pages/mainpage">
            <i class="fa fa-laptop"></i>
			<span>Главная страница</span>            
          </a>          
        </li>
		<li class="{{ $active_menu == 'slider' ? 'active' : '' }}">
          <a href="/dashboard/pages/sider">
            <i class="fa  fa-image"></i>
            <span>Сайдер</span>            
          </a>         
        </li>
        <li class="{{ $active_menu == 'support' ? 'active' : '' }}">
          <a href="/dashboard/pages/support">
            <i class="fa  fa-life-ring"></i>
            <span>Поддержка</span>            
          </a>         
        </li>
		<li class="{{ $active_menu == 'team' ? 'active' : '' }}">
          <a href="/dashboard/team">
            <i class="fa  fa-users"></i>
            <span>Команда</span>            
          </a>         
        </li>
        <li class="treeview {{ (in_array($active_menu, ['createAcc','login','forgotpassword','lk'])) ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-user-o"></i>
            <span>Авторизация</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ $active_menu == 'createAcc' ? 'active' : '' }}">
				<a href="{{ route('dashboard.pages.edit', ['slug' => 'createacc']) }}"><i class="fa fa-circle-o"></i> Регистрация</a>
			</li>
			<li class="{{ $active_menu == 'login' ? 'active' : '' }}">
				<a href="{{ route('dashboard.pages.edit', ['slug' => 'login']) }}"><i class="fa fa-circle-o"></i> Авторизация</a>
			</li>
			<li class="{{ $active_menu == 'forgotpassword' ? 'active' : '' }}">
				<a href="{{ route('dashboard.pages.edit', ['slug' => 'forgotpassword']) }}"><i class="fa fa-circle-o"></i> Восстановление пароля</a>
			</li>
			<li class="{{ $active_menu == 'lk' ? 'active' : '' }}">
				<a href="{{ route('dashboard.pages.edit', ['slug' => 'lk']) }}"><i class="fa fa-circle-o"></i> Личный кабинет</a>
			</li>
          </ul>
        </li>
        
        <li class="header">Игры</li>
		<li class="treeview {{ (in_array($active_menu, ['game-mechattacks','game-mechattacks-weapons','game-mechattacks-research','mech'])) ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-gamepad"></i>
            <span>Mechattacks</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ $active_menu=='game-mechattacks' ? 'active' : '' }}"><a href="/dashboard/game/mechattacks"><i class="fa fa-puzzle-piece"></i> Игра</a></li>
			<li class="{{ $active_menu=='game-mechattacks-weapons' ? 'active' : '' }}"><a href="/dashboard/game/mechattacks/weapons"><i class="fa fa-fire"></i> Оружие</a></li>			
			<li class="{{ $active_menu=='game-mechattacks-research' ? 'active' : '' }}"><a href="/dashboard/game/mechattacks/research"><i class="fa fa-fire"></i> Исследования</a></li>
			<li class="{{ $active_menu=='mech' ? 'active' : '' }}"><a href="/dashboard/mech"><i class="fa fa-fire"></i> Мехи</a></li>
          </ul>
        </li>
		
		<li class="treeview {{ (in_array($active_menu, ['game-boomboom','game-boomboom-pump','game-boomboom-raids','game-boomboom-infographics','heroes'])) ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-gamepad"></i>
            <span>BoomBoom</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ $active_menu=='game-boomboom' ? 'active' : '' }}"><a href="{{ route('dashboard.game.index',['boomboom']) }}"><i class="fa fa-puzzle-piece"></i> Игра</a></li>			
			<li class="{{ $active_menu=='game-boomboom-pump' ? 'active' : '' }}"><a href="{{ route('dashboard.game.section',['boomboom','pump']) }}"><i class="fa fa-fire"></i> Pump</a></li>
			<li class="{{ $active_menu=='game-boomboom-raids' ? 'active' : '' }}"><a href="{{ route('dashboard.game.section',['boomboom','raids']) }}"><i class="fa fa-fire"></i> Raids</a></li>
			<li class="{{ $active_menu=='game-boomboom-infographics' ? 'active' : '' }}"><a href="{{ route('dashboard.game.section',['boomboom','infographics']) }}"><i class="fa fa-fire"></i> Инфографика</a></li>
			<li class="{{ $active_menu=='heroes' ? 'active' : '' }}"><a href="{{ route('dashboard.game.hero.index') }}"><i class="fa fa-fire"></i> Герои</a></li>
          </ul>
        </li>
		
		<li class="header">Пользователи</li>
		<li class="{{ (in_array($active_menu, ['users'])) ? 'active' : '' }}">
			<a href="{{ route('dashboard.users') }}"><i class="fa fa-users"></i> <span> Список пользователей</span></a>
		</li>
		
		<li class="header">Обратная связь</li>
		<li class="{{ ($active_menu == 'feedback') ? 'active' : '' }}">
			<a href="{{ route('dashboard.feedback') }}"><i class="fa fa-question-circle-o "></i> Вопросы в техподдержку</a>        
        </li>
		<li class="{{ ($active_menu == 'bugs') ? 'active' : '' }}">
			<a href="{{ route('dashboard.bugs') }}"><i class="fa fa-bug"></i> Ошибки из игры</a>        
        </li>
		
		<li class="header">Настройки</li>
		<li class="treeview {{ (in_array($active_menu, ['settings'])) ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-gears"></i>
            <span>Настройки</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ $active_menu=='settings' ? 'active' : '' }}"><a href="{{ route('dashboard.settings') }}"><i class="fa fa-code"></i> Настройки</a></li>            
          </ul>
        </li>
		
		<li class="header">Интеграция</li>
        <li><a href="{{ asset('/documentation/index.html')}}" target="_blank"><i class="fa fa-book"></i> <span>Документация API</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
		@yield('page-title')
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Главная</a></li>
		@hasSection('nav-layout') @yield('nav-layout')@endif
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @yield('main')      
      <!-- /.box -->
	   <div class="modal modal-danger fade" id="modal-danger">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Danger Modal</h4>
              </div>
              <div class="modal-body">
                <p>One fine body&hellip;</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Понятно</button>                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		
		<div class="modal modal-warning fade" id="modal-warning">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Предупреждение!</h4>
              </div>
              <div class="modal-body">
                <p>One fine body&hellip;</p>
              </div>
              <div class="modal-footer">               
				<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-outline">Подтверждаю</button>                            
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		
		<div class="modal modal-info fade" id="modal-info">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Информация!</h4>
              </div>
              <div class="modal-body">
                <p>One fine body&hellip;</p>
              </div>
              <div class="modal-footer">               
				<button type="button" class="btn btn-outline" data-dismiss="modal">Понятно</button>                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		
		<div class="modal modal-default fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Информация!</h4>
              </div>
              <div class="modal-body">
                <p>One fine body&hellip;</p>
              </div>
              <div class="modal-footer">               
				<button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-success js-game-add">Сохранить</button>                            
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


 
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
@endsection