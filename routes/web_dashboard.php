<?php

Route::get('/', 'DashboardController@index')->name('dashboard');

// Account
Route::get('/account', 'AccountController@edit')->name('dashboard.account.edit');
Route::post('/account', 'AccountController@update')->name('dashboard.account.update');

//Route::get('/logout', 'UsersController@logout')->name('dashboard.logout');

// Users - Back-end
Route::get('/users', 'UsersController@index')->name('dashboard.users');
Route::delete('/users/{user}', 'UsersController@destroy')->name('dashboard.users.delete');

// Pages - Front-end
Route::get('/pages/{slug}', 'PagesController@edit')->name('dashboard.pages.edit');

// Pages - Back-end
Route::post('/pages/{page}', 'PagesController@update')->name('dashboard.pages.update');

// Teams
Route::get('/team', 'TeamController@show')->name('dashboard.team.show');
Route::get('/team/create', 'TeamController@create')->name('dashboard.team.create');
Route::get('/team/{team}/edit', 'TeamController@edit')->name('dashboard.team.edit');

// Teams Back-end
Route::post('/team', 'TeamController@store')->name('dashboard.team.store');
Route::post('/team/{team}', 'TeamController@update')->name('dashboard.team.update');
Route::delete('/team',  'TeamController@destroy')->name('dashboard.team.destroy');

// ALL images Back-end
Route::post('/images/delete', 'TeamImagesController@delete')->name('dashboard.team.image.delete');
Route::post('/image/delete', 'TeamController@delete')->name('dashboard.team.delete');
Route::post('/complex-image/delete', 'GameComplexController@delete')->name('dashboard.game-complex.delete');
Route::post('/weapons-image/delete/{id}', 'HeroWeaponsController@delete')->name('dashboard.weapons-image.delete');


// Games
Route::get('/game/add', 'GameController@add')->name('dashboard.game.add'); //  получает конкретную форму для конкретного типа данных
Route::get('/game/{slug}', 'GameController@index')->name('dashboard.game.index');

// Games items Back-end
Route::get('/game-item/{id}', 'GameItemController@index')->name('dashboard.game.item.index');
Route::post('/game-item/store', 'GameItemController@store')->name('dashboard.game.item.store');
Route::delete('/game-item/{id}', 'GameItemController@destroy')->name('dashboard.game.item.destroy');
Route::post('/game-item/image/{id}', 'GameItemController@delimage')->name('dashboard.game.item.image.delimage');

// Games complex-item Front-end
Route::get('/game/{slug}/{section}', 'GameController@section')->name('dashboard.game.section');
Route::get('/game-complex-item/{id}', 'GameComplexItemController@index')->name('dashboard.game.complex.item.index');

Route::get('/game-complex/add', 'GameComplexItemController@add')->name('dashboard.game.complex.add'); //  получает конкретную форму для конкретного типа данных
Route::post('/game-complex-item/store', 'GameComplexItemController@store')->name('dashboard.game.complex.item.store');
Route::delete('/game-complex-item/{id}', 'GameComplexItemController@destroy')->name('dashboard.game.complex.item.destroy');

Route::post('/game-complex-item/image/{id}', 'GameComplexItemController@delimage')->name('dashboard.game.complex.item.delimage');

// Mech
Route::get('/mech', 'GameRobotController@index')->name('dashboard.game.robot.index');
Route::get('/mech/create', 'GameRobotController@edit')->name('dashboard.game.robot.edit');
Route::get('/mech/{robot}', 'GameRobotController@edit')->name('dashboard.game.robot.edit');
Route::get('/mech/{robot}/copy', 'GameRobotController@copyMech')->name('dashboard.game.robot.copy');


Route::delete('/mech/{id}',  'GameRobotController@destroy')->name('dashboard.game.robot.destroy');
Route::post('/mech/store', 'GameRobotController@store')->name('dashboard.game.robot.store');
Route::post('/mech/{robot}/update', 'GameRobotController@update')->name('dashboard.game.robot.update');
Route::post('/mech/delete/{id}', 'GameRobotController@delimage')->name('dashboard.game.robot.delimage');


// Table columns
Route::get('/mech/line/form/{id?}', 'RobotTableController@show')->name('dashboard.robot.table.show');
Route::get('/mech/{robot}/table', 'RobotTableController@table')->name('dashboard.robot.table');

Route::delete('/mech/line/{id}',  'RobotTableController@destroy')->name('dashboard.robot.table.destroy');
Route::post('/mech/line/store', 'RobotTableController@store')->name('dashboard.robot.table.store');


// Heroes
Route::get('/heroes', 'GameHeroController@index')->name('dashboard.game.hero.index');
Route::get('/heroes/create', 'GameHeroController@edit')->name('dashboard.game.hero.edit');
Route::get('/heroes/{hero}', 'GameHeroController@edit')->name('dashboard.game.hero.edit');
Route::get('/heroes/{hero}/copy', 'GameHeroController@copyHero')->name('dashboard.game.hero.copy');


Route::delete('/heroes/{id}',  'GameHeroController@destroy')->name('dashboard.game.hero.destroy');
Route::post('/heroes/store', 'GameHeroController@store')->name('dashboard.game.hero.store');
Route::post('/heroes/{hero}/update', 'GameHeroController@update')->name('dashboard.hero.robot.update');
Route::post('/heroes/delete/{id}', 'GameHeroController@delimage')->name('dashboard.game.hero.delimage');


// Weapons hero
Route::get('/weapons/{weapon?}', 'HeroWeaponsController@show')->name('dashboard.hero.weapons.show');
Route::get('/heroes/{hero}/weapons', 'HeroWeaponsController@table')->name('dashboard.weapons.table');

Route::delete('/heroes/weapons/{id}',  'HeroWeaponsController@destroy')->name('dashboard.hero.weapons.destroy');
Route::post('/heroes/weapons/store', 'HeroWeaponsController@store')->name('dashboard.hero.weapons.store');

// App Settings
Route::get('/settings', 'AppSettingsController@index')->name('dashboard.settings');
Route::post('/settings', 'AppSettingsController@update')->name('dashboard.settings.update');

// Feedback frontend
Route::get('/feedback', 'FeedbackController@index')->name('dashboard.feedback');
Route::delete('/feedback/{id}', 'FeedbackController@destroy')->name('dashboard.feedback.destroy');

// Bugs frontend
Route::get('/bugs', 'BugsController@index')->name('dashboard.bugs');
Route::delete('/bugs/{id}', 'BugsController@destroy')->name('dashboard.bugs.destroy');

// Roles ajax
Route::get('/roles/{language}', 'RolesController@index')->name('dashboard.roles');

