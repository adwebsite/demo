<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication system
Route::post('/auth/register', 'AuthController@register')->name('api.auth.register');
Route::post('/auth/login', 'AuthController@login')->name('api.auth.login');
Route::post('/auth/password/reset', 'AuthController@sendPasswordResetLink')->name('api.auth.password.reset');

Route::post('/auth/social', 'AuthController@social')->name('api.auth.social');

// Получение данных страниц
Route::get('/data/{slug}', 'DataController@fullpage')->name('api.data.fullpage');
// игры
Route::get('/game/{slug}', 'DataController@game')->name('api.game');
// Список языков
Route::get('/languages', 'DataController@languages')->name('api.languages');
// Список адресов
Route::get('/address', 'DataController@address')->name('api.address');


// Authenticated requests
Route::middleware('auth:api')->group(function () {
    // User Profile
    Route::post('/profile', 'ProfileController@update')->name('api.profile.update');
	Route::get('/profile', 'ProfileController@profile')->name('api.profile.profile');

    // User Settings
    Route::post('/settings', 'UserSettingsController@update')->name('api.settings.update');
});
