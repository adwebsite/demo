<?php

use Faker\Generator as Faker;
use App\UserSettings;

$factory->define(UserSettings::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'push_notifications_on' => false,
    ];
});
