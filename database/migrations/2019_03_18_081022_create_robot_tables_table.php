<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRobotTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('robot_tables', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('robot_id');			
			$table->string('type');
			$table->string('columns');
			
			$table->foreign('robot_id')
                ->references('id')
                ->on('game_robots')
                ->onDelete('CASCADE');
			
			/*$table->foreign('language')
                ->references('slug')
                ->on('languages')
                ->onDelete('CASCADE');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('robot_tables');
    }
}
