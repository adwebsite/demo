<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameRobotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_robots', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('game_id');
			$table->string('language')->default('EN');
			$table->string('slug');
			$table->string('name');
			$table->string('image')->nullable();
			$table->string('role')->nullable();
			$table->text('description')->nullable();
			$table->string('tableTitle')->nullable();			
			$table->unsignedInteger('max_col')->default(4);
			
			$table->foreign('game_id')
                ->references('id')
                ->on('games')
                ->onDelete('CASCADE');
			
			$table->foreign('language')
                ->references('slug')
                ->on('languages')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_robots');
    }
}
