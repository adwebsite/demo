<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\GameHero;

class ModifyGameHeroesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		GameHero::query()->update(['role' => NULL]);
		//DB::statement('ALTER TABLE game_heroes CHANGE role role INT UNSIGNED  DEFAULT NULL)');
        Schema::table('game_heroes', function (Blueprint $table) {
            $table->string('role',50)->nullable()->change();
			
			$table->foreign('role')
                ->references('key')
                ->on('roles')
                ->onDelete('set NULL');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_heroes', function (Blueprint $table) {
			$table->dropForegin(['role']);
            $table->string('role',255)->nullable()->change();
        });
    }
}
