<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameBugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_bugs', function (Blueprint $table) {
            $table->increments('id');
			$table->string('deviceId');			
			$table->string('playerName');
			$table->string('deviceInfo')->nullable();
			$table->string('messageType');
			$table->string('gameState');
			$table->string('message');
			$table->string('version');
            $table->timestamps();		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_bugs');
    }
}
