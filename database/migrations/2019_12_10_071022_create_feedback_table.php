<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->increments('id');
			$table->string('game_slug');			
			$table->text('questions');			
			$table->text('message')->nullable();
			$table->string('name')->nullable();
			$table->string('email')->nullable();
            $table->timestamps();			
			
			$table->foreign('game_slug')
                ->references('slug')
                ->on('games')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
