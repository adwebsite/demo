<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_items', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('page_id');
			$table->string('language')->default(1);
			$table->string('slug');    
			$table->string('type');// string/text/image
            $table->text('value');
			
			$table->foreign('page_id')
                ->references('id')
                ->on('pages')
                ->onDelete('CASCADE');
			
			$table->foreign('language')
                ->references('slug')
                ->on('languages')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_items');
    }
}
