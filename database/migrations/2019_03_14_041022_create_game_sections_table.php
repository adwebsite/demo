<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_sections', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('game_id');			
			$table->string('slug');
			$table->string('name')->nullable();
			
			$table->foreign('game_id')
                ->references('id')
                ->on('games')
                ->onDelete('CASCADE');			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_sections');
    }
}
