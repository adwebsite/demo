<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeroWeaponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hero_weapons', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('hero_id');			
			$table->string('title')->nullable();			
			$table->text('text')->nullable();
			$table->string('image')->nullable();
			
			$table->foreign('hero_id')
                ->references('id')
                ->on('game_heroes')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hero_weapons');
    }
}
