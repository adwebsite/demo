<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameComplexItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_complex_items', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('game_section_id');
			$table->string('language')->default('EN');			
			$table->string('slug');
			$table->string('title')->nullable();			
			$table->text('text')->nullable();
			$table->string('image')->nullable();
			
			$table->foreign('game_section_id')
                ->references('id')
                ->on('game_sections')
                ->onDelete('CASCADE');
			
			$table->foreign('language')
                ->references('slug')
                ->on('languages')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_complex_items');
    }
}
