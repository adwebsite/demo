<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameHeroesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_heroes', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('game_id');
			$table->string('language')->default('EN');
			$table->string('slug');
			$table->string('name');
			$table->string('image')->nullable();
			$table->string('role')->nullable();
			$table->text('description')->nullable();
			$table->string('weaponsTitle')->nullable();			
			
			$table->foreign('game_id')
                ->references('id')
                ->on('games')
                ->onDelete('CASCADE');
			
			$table->foreign('language')
                ->references('slug')
                ->on('languages')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_heroes');
    }
}
