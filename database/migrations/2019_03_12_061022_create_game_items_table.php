<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_items', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('game_id');
			$table->string('language')->default('EN');
			$table->string('type');// string, image, array, text, weapons, research, robots
			$table->string('slug');
			$table->text('value');
			
			$table->foreign('game_id')
                ->references('id')
                ->on('games')
                ->onDelete('CASCADE');
			
			$table->foreign('language')
                ->references('slug')
                ->on('languages')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_items');
    }
}
