<?php

use Illuminate\Database\Seeder;
use App\Role;
use Illuminate\Support\Facades\Schema;


class RolesSeeder extends Seeder
{
    /**
     * Run the Roles seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        
		Role::truncate();        
        
		$arr = [
			[			
				'language' 		=> 'EN',
				'key' 			=> 'light',
				'title' 		=> 'light',
				'game_id'		=> 1,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'EN',
				'key' 			=> 'middle',
				'title' 		=> 'middle',
				'game_id'		=> 1,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'EN',
				'key' 			=> 'heavy',
				'title' 		=> 'heavy',
				'game_id'		=> 1,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'EN',
				'key' 			=> 'all',
				'title' 		=> 'all',
				'game_id'		=> 1,
				'visible'		=> 0
			],
			[			
				'language' 		=> 'RU',
				'key' 			=> 'light',
				'title' 		=> 'легкий',
				'game_id'		=> 1,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'RU',
				'key' 			=> 'middle',
				'title' 		=> 'средний',
				'game_id'		=> 1,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'RU',
				'key' 			=> 'heavy',
				'title' 		=> 'тяжелый',
				'game_id'		=> 1,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'RU',
				'key' 			=> 'all',
				'title' 		=> 'Все',
				'game_id'		=> 1,
				'visible'		=> 0
			],
			[			
				'language' 		=> 'EN',
				'key' 			=> 'tank',
				'title' 		=> 'tank',
				'game_id'		=> 2,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'EN',
				'key' 			=> 'damage',
				'title' 		=> 'damage',
				'game_id'		=> 2,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'EN',
				'key' 			=> 'support',
				'title' 		=> 'support',
				'game_id'		=> 2,
				'visible'		=> 1
			],		
			[			
				'language' 		=> 'EN',
				'key' 			=> 'all',
				'title' 		=> 'all',
				'game_id'		=> 2,
				'visible'		=> 0
			],
			[			
				'language' 		=> 'RU',
				'key' 			=> 'tank',
				'title' 		=> 'Танк',
				'game_id'		=> 2,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'RU',
				'key' 			=> 'damage',
				'title' 		=> 'урон',
				'game_id'		=> 2,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'RU',
				'key' 			=> 'support',
				'title' 		=> 'поддержка',
				'game_id'		=> 2,
				'visible'		=> 1
			],
			[			
				'language' 		=> 'RU',
				'key' 			=> 'all',
				'title' 		=> 'Все',
				'game_id'		=> 2,
				'visible'		=> 0
			],
			
		];
		
		foreach ($arr AS $item) {
			Role::create($item);
		}
                
        Schema::enableForeignKeyConstraints();
    }
	
}
