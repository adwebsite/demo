<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\AppSettings;
use Illuminate\Support\Facades\Schema;


class AppSettingsSeeder extends Seeder
{
    /**
     * Run the AppSettings seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        
		AppSettings::truncate();        
        
		AppSettings::create([
			'user_id' 	=> 1,
			'slug' 		=> 'secret_key',
			'title' 	=> 'Секретный ключ API',
			'value' 	=> 'asdVsf$@f4r3_dfDDAS'
		]);
                
        Schema::enableForeignKeyConstraints();
    }
	
}
