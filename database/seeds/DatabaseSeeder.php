<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(InitialDataSeeder::class);
		$this->call(AppSettingsSeeder::class);
		$this->call(RolesSeeder::class);
    }
}
